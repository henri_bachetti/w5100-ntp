
#include <SPI.h>
#include <Ethernet.h>
#include <Dns.h>
#include <w5100-ntp.h>

#define GET                   1
#define POST                  2
#define FORM                  POST

#define REQUEST_MAX           150
#define URL_MAX               100
#define ARGSTR_MAX            80
#define ARG_MAX               5

byte mac[] = {
  0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED
};
IPAddress ip(192, 168, 1, 177);
IPAddress dnsIp(192, 168, 1, 1);
const char *ntpUrl = "pool.ntp.org";
IPAddress ntpIp = EthernetNtp::defaultTimeServer;

EthernetServer server(80);
DNSClient dns;

struct cgi_arg
{
  char *name;
  char *value;
};

void setup()
{
  EthernetNtp *ntp = EthernetNtp::getInstance();
  Serial.begin(115200);
  Serial.println(F("Network Time Protocol Example"));
  Ethernet.begin(mac, ip, dnsIp);
  if (Ethernet.hardwareStatus() == EthernetNoHardware) {
    Serial.println(F("Ethernet hardware was not found.  Sorry :("));
    while (true) {
      delay(1);
    }
  }
  if (Ethernet.linkStatus() == LinkOFF) {
    Serial.println(F("Ethernet cable is not connected."));
  }
  // Europe/Paris : last sunday of october at 3:00 : 1H offset
  ntp->std("CET", Last, Sun, Oct, 3, 60);
  // Europe/Paris : last sunday of march at 2:00 : 2H offset
  ntp->dst("CEST", Last, Sun, Mar, 2, 120);
  dns.begin(dnsIp);
  // get NTP server using DNS
  Serial.print(F("DNS (")); Serial.print(ntpUrl); Serial.print(F("): "));
  Serial.println(dns.getHostByName(ntpUrl, ntpIp) == true ? "SUCCESS" : "FAILED");
  // update time every 60 seconds
#ifdef USE_RTCLIB
  ntp->addDS3231();
#endif
  ntp->begin(ntpIp, 60);
  Serial.print(F("server is at "));
  Serial.println(Ethernet.localIP());
}

char *_type;
char _fullUrl[URL_MAX];
char *_url;
char *_proto;
char *_ip;
char *_agent;
struct cgi_arg _args[ARG_MAX];

unsigned char h2int(char c)
{
  if (c >= '0' && c <= '9') return ((unsigned char)c - '0');
  if (c >= 'a' && c <= 'f') return ((unsigned char)c - 'a' + 10);
  if (c >= 'A' && c <= 'F') return ((unsigned char)c - 'A' + 10);
  return (0);
}

/*
   decode escaped chars like %20, etc
*/
const char *decode(const char *src, char *dest, size_t size)
{
  int i = 0;
  char code0;
  char code1;

  while (*src++) {
    if (*src == '%' && *(src + 1) != 0 && *(src + 2) != 0) {
      src++;
      code0 = *src;
      src++;
      code1 = *src;
      if (i < size) {
        dest[i++] = (h2int(code0) << 4) | h2int(code1);
      }
    }
    else {
      if (i < size) {
        dest[i++] = *src;
      }
    }
  }
  dest[i] = '\0';
  return dest;
}

/*
   return argument value for a given name
*/
const char *getArg(const char *name)
{
  for (int i = 0 ; i < ARG_MAX ; i++) {
    if (_args[i].name && !strcmp(_args[i].name, name)) {
      return _args[i].value;
    }
  }
  return NULL;
}

/*
   parse HTTP request
*/
void parseRequest(char *request, int mode, char *argsStr)
{
  char *urlStart, latToken;

  char *token = strtok(request, " ");
  if (token != NULL) {
    _type = token;
  }
  token = strtok(NULL, " ");
  if (token != NULL) {
    urlStart = token;
    strcpy(_fullUrl, token);
    Serial.print(F("URL : ")); Serial.println(_fullUrl);
  }
  token = strtok(NULL, "/");
  token = strtok(NULL, "\n");
  if (token != NULL) {
    _proto = token;
  }
  token = strtok(NULL, " ");
  token = strtok(NULL, "\n");
  if (token != NULL) {
    _ip = token;
  }
  token = strtok(NULL, " ");
  token = strtok(NULL, "\n");
  if (token != NULL) {
    _agent = token;
  }
  token = strtok(urlStart, "?");
  if (token != NULL) {
    _url = token;
  }
  memset(_args, 0, sizeof(_args));
  if (mode == POST) {
    token = strtok(argsStr, "=");
  }
  else {
    token = strtok(NULL, "=");
  }
  int i = 0;
  while (i < ARG_MAX && token != NULL) {
    _args[i].name = token;
    token = strtok(NULL, "&");
    if (token != NULL) {
      _args[i++].value = token;
    }
    token = strtok(NULL, "=");
  }
  for (int i = 0 ; i < ARG_MAX ; i++) {
    if (_args[i].name) {
      Serial.print(_args[i].name); Serial.print(F("=")); Serial.println(_args[i].value);
    }
  }
}

void handleNotFound(EthernetClient &client)
{
  IPAddress addr = Ethernet.localIP();
  char ip[16];

  for (int i = 0; i < 4 ; i++) {
    sprintf(_ip, "%d.%d.%d.%d", addr[0], addr[1], addr[2], addr[3]);
  }
  const char *htmlHeader = "<!DOCTYPE HTML PUBLIC>\n<html><head>\n<title>404 Not Found</title>\n</head><body>\n";
  client.println(htmlHeader);
  client.println(F("<h1>Not Found</h1>\n<p>The requested URL "));
  client.print(_url);
  client.println(F(" was not found on this server.</p>\n<hr>\n<address>Arduino Server at "));
  client.print(_ip); client.print(F(" Port ")); client.print(client.localPort());
  client.print(F("</address>\n</body></html>"));
}

void handleGetRequest(EthernetClient &client, char *request)
{
  parseRequest(request, GET, 0);
  if (strcmp(_url, "/time") == 0) {
    char buf[30];
    client.println(F("HTTP/1.1 200 OK"));
    client.println(F("Content-Type: text/html"));
    client.println(F("Connection: close"));
    client.println();
    client.println(F("<!DOCTYPE HTML>"));
    client.println(F("<html>"));
    client.print("Using standard C library:");
    client.println(F("<br />"));
    client.print("UTC time & date: ");
    time_t t;
    time(&t);
    Serial.print(F("system time: ")); Serial.println(t);
    struct tm *current = gmtime(&t);
    const char *format = "%A %d/%m/%Y, %H:%M:%S";
    strftime(buf, sizeof(buf), format, current);
    client.print(buf);
    client.println(F("<br />"));
    client.print("Local time & date: ");
    current = localtime(&t);
    strftime(buf, sizeof(buf), format, current);
    client.print(buf);
    client.println(F("<br /><br />"));
    client.print("Using formattedTime:");
    client.println(F("<br />"));
    EthernetNtp *ntp = EthernetNtp::getInstance();
    ntp->formattedTime(buf, sizeof(buf), format);
    client.print("Local time & date: ");
    client.print(buf);
#ifdef USE_RTCLIB
    client.println(F("<br /><br />"));
    client.print("Using RTC:");
    client.println(F("<br />"));
    DateTime now(ntp->rtcNow());
    strcpy(buf, "DDD DD/MM/YYYY hh:mm:ss");
    now.toString(buf);
    client.print("Local time & date: ");
    client.print(buf);
#endif
    client.println(F("<br />"));
    client.println(F("</html>"));
  }
  else {
    handleNotFound(client);
  }
}

int handlePostRequest(EthernetClient &client, char *request, char *argStr)
{
  parseRequest(request, POST, argStr);
  handleNotFound(client);
}

void loop()
{
  static char request[REQUEST_MAX + 1];
  static unsigned long lastTick;
  int reqIndex = 0;
  char c;
  EthernetClient client = server.available();

  unsigned long tick = millis();
  if (tick - lastTick > 1000) {
    EthernetNtp::getInstance()->tick();
    lastTick = tick;
  }
  if (client) {
    Serial.println(F("new client"));
    boolean currentLineIsBlank = true;
    while (client.connected()) {
      if (client.available()) {
        c = client.read();
        if (reqIndex < REQUEST_MAX) {
          request[reqIndex++] = c;
          request[reqIndex] = 0;
        }
        if (c == '\n' && currentLineIsBlank && !strncmp(request, "GET", 3)) {
          Serial.print(F("Total-Length=")); Serial.println(reqIndex);
          Serial.println(request);
          handleGetRequest(client, request);
          break;
        }
        if (c == '\n' && currentLineIsBlank && !strncmp(request, "POST", 4)) {
          char argStr[ARGSTR_MAX + 1];
          int i = 0;
          while (c > 0) {
            c = client.read();
            if (c > 0 && i < ARGSTR_MAX) {
              argStr[i++] = c;
              argStr[i] = 0;
            }
            else {
              Serial.println(F("buffer is OVER"));
            }
          }
          Serial.println(request);
          Serial.print(F("Total-Length=")); Serial.println(reqIndex);
          Serial.print("ARGS="); Serial.println(argStr);
          handlePostRequest(client, request, argStr);
          break;
        }
      }
      if (c == '\n') {
        currentLineIsBlank = true;
      }
      else if (c != '\r') {
        currentLineIsBlank = false;
      }
    }
    delay(1);
    client.stop();
    Serial.println(F("client disconnected"));
  }
}
