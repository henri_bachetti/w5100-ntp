# ARDUINO NTP LIBRARY for W5100 Ethernet WEB SERVER 

w5100-ntp is an NTP library for ARDUINO using a W5100 Ethernet board.

The project uses the following components :

 * an ARDUINO UNO, NANO or MEGA
 * a W5100 Ethernet board

The library initializes the time for the TimeLib and the standard C library time system, so it's possible to get the time using different methods :

 * timelib : year(), month(), day(), hour(), minute(), seconds(), etc.
 * std C libraty : time(), gmtime(), localtime(), etc.

Of course timezones can be used to get the summer time and winter time.

### DEPENDENCY

The TimeLib is needed : https://github.com/PaulStoffregen/Time.git

### INTERFACE

**getInstance()**

Gets the EthernetNtp instance.

**time_t getNtpTime(void);**

Request for the NTP time. Normally the application does not have to call this method.

**void begin(IPAddress &address, time_t syncInterval);**

Initializes the library, giving the NTP server address and time synchronization interval.

Default values are pool.ntp.org and 1 hour

The adresse of the getNtpTime() method is used as synchronization provider for the TimeLib :

setSyncProvider(getNtpTime);

setSyncInterval(syncInterval);

**bool addDS3231(void);**

Adds a DS3231 RTC.

This module will be updated with UTC time each time the NTP is available.

Later, if there is no response from NTP, the RTC time will be requested.

**void std(const char* tzName, int8_t week, int8_t wday, int8_t month, int8_t hour, int tzOffset);**

Initializes the standard timezone.

Example for Europe/Paris:
```
  // standard timezone : last sunday of october at 3H00 : +60 minutes
  EthernetNtp *ntp = EthernetNtp::getInstance();
  ntp->std("CET", Last, Sun, Oct, 3, 60);
```

**void dst(const char* tzName, int8_t week, int8_t wday, int8_t month, int8_t hour, int tzOffset);**

Initializes the summer timezone.

Example for Europe/Paris:
```
  // summer timezone : last sunday of march at 2H00 : +120 minutes
  EthernetNtp *ntp = EthernetNtp::getInstance();
  ntp->dst("CEST", Last, Sun, Mar, 2, 120);
```

**char *formattedTime(char *s, size_t maxsize, const char *format);**

Formats a string with a strftime() format.

The strftime() format is explained here : http://www.cplusplus.com/reference/ctime/strftime/

This is a short cut to obtain a string without calling strftime().

Example:
```
    char buf[30];
    time_t t;
    time(&t);
    // C library method
    struct tm *current = localtime(&t);
    const char *format = "%A %d/%m/%Y, %H:%M:%S";
    strftime(buf, sizeof(buf), format, current);

    // NTP library method
    EthernetNtp *ntp = EthernetNtp::getInstance();
    ntp->formattedTime(buf, sizeof(buf), format);
```

### OPTIONS

TimeLib and RtcLib are optional (see w5100-ntp.h) :

```
// uncomment to use TimeLib
//#define USE_TIMELIB

// uncomment to use RtcLib
//#define USE_RTCLIB
```


### ARDUINO

The code is build using ARDUINO IDE 1.8.11.

### BLOG
A description in french here : https://riton-duino.blogspot.com/2020/04/arduino-ntp-sur-ethernet.html

