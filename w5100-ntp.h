
#ifndef _W5100_NTP_H_
#define _W5100_NTP_H_

// uncomment to use TimeLib
//#define USE_TIMELIB

// uncomment to use RtcLib
//#define USE_RTCLIB

#include <time.h>
#include <EthernetUdp.h>

#ifdef USE_TIMELIB
#include <TimeLib.h>
#else
#define SECS_PER_MIN  ((time_t)(60UL))
#define SECS_PER_HOUR ((time_t)(3600UL))
#define SECS_PER_DAY  ((time_t)(SECS_PER_HOUR * 24UL))
#endif
#ifdef USE_RTCLIB
#include "RTClib.h"
#endif

#define SECS_PER_MINUTES 60

const int NTP_PACKET_SIZE = 48; // NTP time is in the first 48 bytes of message

enum week_t {Last, First, Second, Third, Fourth};
enum dow_t {Sun, Mon, Tue, Wed, Thu, Fri, Sat};
enum month_t {Jan, Feb, Mar, Apr, May, Jun, Jul, Aug, Sep, Oct, Nov, Dec};

struct ruleDST
{
  char timezone[6];
  int8_t week;
  int8_t wday;
  int8_t month;
  int8_t hour;
  int offset;
};

class EthernetNtp
{
  private:
    static EthernetNtp *m_instance;
    static IPAddress m_timeServer;
    static EthernetUDP m_udp;
    static byte m_packetBuffer[NTP_PACKET_SIZE];
    struct ruleDST dstStart, dstEnd;
    time_t dstTime, stdTime;
    uint16_t yearDST;
#ifndef USE_TIMELIB
    unsigned long m_lastTick;
    time_t m_syncInterval;
#endif
#ifdef USE_RTCLIB
    static RTC_DS3231 *m_rtc;
#endif
    EthernetNtp();
    void sendNTPpacket(IPAddress &address);
    int getTimeZoneOffset(time_t utc);
    time_t calcDateDST(struct ruleDST rule, int year);
  public:
    static IPAddress defaultTimeServer;
    static EthernetNtp* getInstance();
    void tick(void);
    static time_t getNtpTime(void);
    void begin(IPAddress &address=defaultTimeServer, time_t syncInterval=3600);
#ifdef USE_RTCLIB
    bool addDS3231(void);
    static time_t rtcNow(void);
#endif
    void dst(const char* tzName, int8_t week, int8_t wday, int8_t month, int8_t hour, int tzOffset);
    void std(const char* tzName, int8_t week, int8_t wday, int8_t month, int8_t hour, int tzOffset);
    char *formattedTime(char *s, size_t maxsize, const char *format);
};

#endif
