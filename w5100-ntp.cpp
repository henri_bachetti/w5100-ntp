
#include "w5100-ntp.h"

unsigned int localPort = 8888;

EthernetNtp *EthernetNtp::m_instance;
IPAddress EthernetNtp::m_timeServer;
EthernetUDP EthernetNtp::m_udp;
byte EthernetNtp::m_packetBuffer[NTP_PACKET_SIZE];

IPAddress EthernetNtp::defaultTimeServer(193, 52, 136, 2); // pool.ntp.org

#ifdef USE_RTCLIB
  RTC_DS3231 *EthernetNtp::m_rtc;
#endif

EthernetNtp::EthernetNtp()
{
#ifndef USE_TIMELIB
  m_lastTick = 0;
#endif
  memset(&dstStart, 0, sizeof(dstStart));
  memset(&dstEnd, 0, sizeof(dstEnd));
}

EthernetNtp *EthernetNtp::getInstance()
{
  if (m_instance == 0) {
    m_instance = new EthernetNtp;
  }
  return m_instance;
}

void EthernetNtp::begin(IPAddress &addr, time_t syncInterval)
{
  m_timeServer = addr;
  m_instance->m_udp.begin(localPort);
  Serial.println(F("waiting for sync"));
#ifdef USE_TIMELIB
  setSyncProvider(getNtpTime);
  setSyncInterval(syncInterval);
#else
  m_syncInterval = syncInterval * 1000;
  tick();
#endif
}

#ifdef USE_RTCLIB

bool EthernetNtp::addDS3231(void)
{
  m_rtc = new RTC_DS3231();
  if (!m_rtc->begin()) {
    Serial.println(F("Couldn't find RTC"));
    delete m_rtc;
    m_rtc = 0;
    return false;
  }
  if (m_rtc->lostPower()) {
    Serial.println(F("RTC lost power"));
    time_t t = getNtpTime();
    if (t > 0) {
      m_rtc->adjust(DateTime(t));
    }
    else {
      m_rtc->adjust(DateTime(F(__DATE__), F(__TIME__)));
    }
  }
  return true;
}

time_t EthernetNtp::rtcNow()
{
  time_t utc = 0, local = 0;

  if (m_rtc != 0) {
    if (!m_rtc->lostPower()) {
      utc = m_rtc->now().unixtime();
      Serial.print(F("RTC unixtime: ")); Serial.println(utc);
    }
  }
  if (utc != 0) {
    int offset = m_instance->getTimeZoneOffset(utc);
    local = utc + offset;
    Serial.print(F("RTC local time: ")); Serial.println(local);
  }
  return local;
}

#endif

time_t EthernetNtp::getNtpTime(void)
{
  unsigned long secsSince1900;
  time_t utc = 0, local = 0;

  while (m_instance->m_udp.parsePacket() > 0) ; // discard any previously received packets
  Serial.println(F("Transmit NTP Request"));
  m_instance->sendNTPpacket(m_timeServer);
  uint32_t beginWait = millis();
  while (millis() - beginWait < 1500) {
    int size = m_instance->m_udp.parsePacket();
    if (size >= NTP_PACKET_SIZE) {
      Serial.println(("Received NTP Response"));
      m_instance->m_udp.read(m_packetBuffer, NTP_PACKET_SIZE);  // read packet into the buffer
      // convert four bytes starting at location 40 to a long integer
      secsSince1900 =  (unsigned long)m_packetBuffer[40] << 24;
      secsSince1900 |= (unsigned long)m_packetBuffer[41] << 16;
      secsSince1900 |= (unsigned long)m_packetBuffer[42] << 8;
      secsSince1900 |= (unsigned long)m_packetBuffer[43];
      utc = secsSince1900 - 2208988800UL;
      Serial.print(F("UTC: ")); Serial.println(utc);
#ifdef USE_RTCLIB
      if (m_rtc != 0) {
        Serial.println(F("adjust RTC"));
        m_rtc->adjust(DateTime(utc));
      }
#endif
    }
  }
  if (utc == 0) {
    Serial.println(F("no NTP Response"));
#ifdef USE_RTCLIB
    if (m_rtc != 0) {
      if (!m_rtc->lostPower()) {
        Serial.println(F("request time from RTC"));
        utc = m_rtc->now().unixtime();
      }
    }
#endif
  }
  if (utc != 0) {
    int offset = m_instance->getTimeZoneOffset(utc);
    struct tm *current = gmtime(&utc);
    current->tm_year -= 30;
    set_zone(0);
    time_t systemTime = mktime(current);
    Serial.print(F("set system time: ")); Serial.println(systemTime);
    set_system_time(systemTime);
    set_zone(offset);
    local = utc + offset;
  }
  Serial.print(F("time: ")); Serial.println(local);
  return local;
}

void EthernetNtp::tick(void)
{
#ifndef USE_TIMELIB
  unsigned long tick = millis();
  if (m_lastTick == 0 || tick - m_lastTick > m_syncInterval) {
    if (getNtpTime() > 0) {
      m_lastTick = tick;
    }
  }
#endif
  system_tick();
}

// send an NTP request to the time server at the given address
void EthernetNtp::sendNTPpacket(IPAddress &address)
{
  memset(m_packetBuffer, 0, NTP_PACKET_SIZE);
  m_packetBuffer[0] = 0b11100011;   // LI, Version, Mode
  m_packetBuffer[1] = 0;     // Stratum, or type of clock
  m_packetBuffer[2] = 6;     // Polling Interval
  m_packetBuffer[3] = 0xEC;  // Peer Clock Precision
  // 8 bytes of zero for Root Delay & Root Dispersion
  m_packetBuffer[12]  = 49;
  m_packetBuffer[13]  = 0x4E;
  m_packetBuffer[14]  = 49;
  m_packetBuffer[15]  = 52;
  m_instance->m_udp.beginPacket(address, 123);
  m_instance->m_udp.write(m_packetBuffer, NTP_PACKET_SIZE);
  m_instance->m_udp.endPacket();
}

void EthernetNtp::std(const char* timezone, int8_t week, int8_t wday, int8_t month, int8_t hour, int offset) {
  strcpy(dstEnd.timezone, timezone);
  dstEnd.week = week;
  dstEnd.wday = wday;
  dstEnd.month = month;
  dstEnd.hour = hour;
  dstEnd.offset = offset;
}

void EthernetNtp::dst(const char* timezone, int8_t week, int8_t wday, int8_t month, int8_t hour, int offset) {
  strcpy(dstStart.timezone, timezone);
  dstStart.week = week;
  dstStart.wday = wday;
  dstStart.month = month;
  dstStart.hour = hour;
  dstStart.offset = offset;
}

int EthernetNtp::getTimeZoneOffset(time_t utc)
{
  struct tm *current = gmtime(&utc);
  time_t utcSTD, utcDST;

  if (dstStart.timezone[0]) {
    dstTime = calcDateDST(dstStart, current->tm_year + 1900);
    utcDST = dstTime - (dstEnd.offset * SECS_PER_MINUTES);
    stdTime = calcDateDST(dstEnd, current->tm_year + 1900);
    utcSTD = stdTime - (dstStart.offset * SECS_PER_MINUTES);
    yearDST = current->tm_year + 1900;
    if ((utc > utcDST) && (utc <= utcSTD)) {
      return (dstStart.offset - dstEnd.offset) * SECS_PER_MINUTES + dstEnd.offset * SECS_PER_MINUTES;
    }
    return dstEnd.offset * SECS_PER_MINUTES;
  }
  return 0;
}

time_t EthernetNtp::calcDateDST(struct ruleDST rule, int year)
{
  uint8_t month = rule.month;
  uint8_t week = rule.week;
  if (week == 0) {
    if (month++ > 11) {
      month = 0;
      year++;
    }
    week = 1;
  }
  struct tm tm;
  tm.tm_hour = rule.hour;
  tm.tm_min = 0;
  tm.tm_sec = 0;
  tm.tm_mday = 1;
  tm.tm_mon = month;
  tm.tm_year = year - 1900;
  time_t t = mktime(&tm);

  t += ((rule.wday - tm.tm_wday + 7) % 7 + (week - 1) * 7 ) * SECS_PER_DAY;
  if (rule.week == 0) t -= 7 * SECS_PER_DAY;
  return t;
}

char* EthernetNtp::formattedTime(char *s, size_t maxsize, const char *format)
{
  memset(s, 0, maxsize);
#ifdef USE_TIMELIB
  struct tm current = {second(), minute(), hour(), day(), weekday()-1, month()-1, year()-1900};
  strftime(s, maxsize, format, &current);
#else
  time_t t;
  time(&t);
  struct tm *current = localtime(&t);
  strftime(s, maxsize, format, current);
#endif
  return s;
}

